import { render, screen } from '@testing-library/react';
import About from '../pages/about';
import { Provider } from 'react-redux';
import { store } from '../store/store'; // Import your Redux store

describe('About Page', () => {
  it('renders the About page', async () => {
    render(<Provider store={store}><About /></Provider>);

    // Check if the welcome paragraph is rendered
    const welcomeParagraph = screen.getByText(/Welcome to our platform/i);
    expect(welcomeParagraph).toBeInTheDocument();

    // Check if the vision paragraph is rendered
    const visionParagraph = screen.getByText(/Our vision is to become the leading support collection/i);
    expect(visionParagraph).toBeInTheDocument();

    // Check if the help paragraph is rendered
    const helpParagraph = screen.getByText(/We are here to help you/i);
    expect(helpParagraph).toBeInTheDocument();
  });
});
