# Utiliser l'image de base Node.js
FROM node:14-alpine

# Définir le répertoire de travail dans le conteneur
WORKDIR /app

# Copier les fichiers du projet dans le conteneur
COPY . .

# Installer les dépendances
RUN npm install

# Construire le projet Vue.js pour la production
RUN npm run build

# Exposer le port 80 (ou tout autre port nécessaire pour votre application Vue.js)
EXPOSE 80

# Définir la commande de démarrage de l'application
CMD ["npm", "run", "serve"]