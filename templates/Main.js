import React from 'react'
import Footer from '../components/Footer';
import Navbar from '../components/Navbar';

const Main = (props) => (
    <>
        <Navbar />
        <div id="main_contents">{props.children}</div>
        <Footer />
    </>
)

export default Main;