import React from 'react';

const InfiniteLoopSlider = ({ children, duration, reverse = false }) => {
    return (
        <div className="loop-slider" style={{
            '--duration': `${duration}ms`,
            '--direction': reverse ? 'reverse' : 'normal'
        }}>
            <div className="inner grid gap-6 mx-auto lg:max-w-none lg:mx-0">
                {children}
                {children}
            </div>
        </div>
    );
};

export default InfiniteLoopSlider;
