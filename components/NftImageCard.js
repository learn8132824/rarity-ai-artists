import React from 'react'
import Image from './Image'
import useNextBlurhash from "use-next-blurhash";

const NftImageCard = ({ data, router }) => {
    const [blurDataUrl] = useNextBlurhash(data.hash);
    return (
        <div key={data.src} className={`relative overflow-hidden w-64 mx-auto rounded-lg transform h-96 ${data.class}`}>
            <Image
                src={`${router.basePath}/${data.src}`}
                loader={blurDataUrl}
                alt={`by ${data.author}`}
                title=""
                className="object-cover h-96 w-64"
            />
            <p className={`bottom-2 left-2 right-2 absolute w-fit px-4 py-1.5 bg-black text-xs rounded-full text-white justify-center items-center`}>
                By {data.author}
            </p>
        </div>
    )
}

export default NftImageCard