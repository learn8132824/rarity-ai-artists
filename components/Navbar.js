/* eslint-disable @next/next/no-html-link-for-pages */
import React, { useState } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useDispatch, useSelector } from 'react-redux';
import { request } from '../store/api/user';

const Navbar = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const [expanded, setExpanded] = useState(false);
  const [show, setShow] = useState(false);

  const onLogout = async (e) => {
    e.preventDefault();
    const { error } = await supabase.auth.signOut()
    router.replace('/');
  }

  const handleSignIn = () => {
    dispatch(request('signIn'));
  };

  const onCloseUserSet = () => {
    if (show) {
      document.getElementsByTagName('footer')[0].style.filter = 'none';
      document.getElementById('main_contents').style.filter = 'none';
    } else {
      document.getElementsByTagName('footer')[0].style.filter = 'blur(8px)';
      document.getElementById('main_contents').style.filter = 'blur(8px)';
    }
    setShow(!show);
  };

  return (
    <header className="relative z-50 py-4 sm:py-6">
      <div className="px-4 mx-auto max-w-7xl sm:px-6 lg:px-8">
        <nav className="flex items-center justify-between">
          <div className="shrink-0">
            <Link href="/" title="" className="flex items-center">
              <img className="w-auto h-16" src="/logo.svg" alt="" />
            </Link>
          </div>

          <div className="flex md:hidden">
            <button type="button" className="p-1 text-gray-900 transition-all duration-200 bg-transparent rounded-md hover:bg-gray-900 hover:text-white focus:outline-none focus:ring-2 focus:ring-gray-900 focus:ring-offset-2"
              onClick={() => setExpanded(!expanded)} aria-expanded={expanded}>
              {!expanded && <span aria-hidden="true">
                <svg className="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
                  <path strokeLinecap="round" strokeLinejoin="round" d="M4 6h16M4 12h16M4 18h16" />
                </svg>
              </span>}

              {expanded && <span aria-hidden="true">
                <svg className="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
                  <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12" />
                </svg>
              </span>}
            </button>
          </div>

          <div className="hidden md:flex md:items-center md:justify-end md:space-x-12 md:mr-auto md:ml-20">
            <Link href="/about" title="" className="text-base font-medium text-gray-500 transition-all duration-200 hover:text-gray-900"> About </Link>

            <a href="/thecollection" title="" className="text-base font-medium text-gray-500 transition-all duration-200 hover:text-gray-900"> The Collection </a>

            <a href="/trends" title="" className="text-base font-medium text-gray-500 transition-all duration-200 hover:text-gray-900"> See the trends </a>

            {user?.currentUser && <a href="/mint" title="" className="text-base font-medium text-gray-500 transition-all duration-200 hover:text-gray-900"> Create </a>}
          </div>

          <div className="hidden md:flex">
            {(!user || (user && !user?.currentUser?.wallet)) && <a
              onClick={handleSignIn}
              title=""
              className="inline-flex items-center justify-center px-4 py-2 text-base font-semibold text-white transition-all duration-200 bg-gray-900 border border-gray-800 rounded-lg focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-900 hover:bg-gray-700"
              role="button"
            >
              Connect Wallet
            </a>}
            {user?.currentUser?.wallet && <a
              onClick={onLogout}
              title=""
              className="inline-flex items-center justify-center px-4 py-2 text-base font-semibold text-white transition-all duration-200 bg-gray-900 border border-gray-800 rounded-lg focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-900 hover:bg-gray-700"
              role="button"
            >
              Logout
            </a>}
          </div>
        </nav>

        {expanded && <nav>
          <div className="px-1 py-5">
            <div className="grid gap-y-6">
              <Link href="/about" title="" className="text-base font-medium text-gray-500 transition-all duration-200 hover:text-gray-900"> About </Link>

              <a href="/thecollection" title="" className="text-base font-medium text-gray-500 transition-all duration-200 hover:text-gray-900"> The Collection </a>

              <a href="/trends" title="" className="text-base font-medium text-gray-500 transition-all duration-200 hover:text-gray-900"> See the trends </a>

              {user && <a href="/mint" title="" className="text-base font-medium text-gray-500 transition-all duration-200 hover:text-gray-900"> Create </a>}

              {(!user || (user && !user?.currentUser?.wallet)) && <a
                onClick={handleSignIn}
                title=""
                className="inline-flex items-center justify-center px-4 py-2 text-base font-semibold text-white transition-all duration-200 bg-gray-900 border border-gray-800 rounded-lg focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-900 hover:bg-gray-700"
                role="button"
              >
                Connect Wallet
              </a>}
              {user?.currentUser?.wallet && <a
                onClick={onLogout}
                title=""
                className="inline-flex items-center justify-center px-4 py-2 text-base font-semibold text-white transition-all duration-200 bg-gray-900 border border-gray-800 rounded-lg focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-900 hover:bg-gray-700"
                role="button"
              >
                Logout
              </a>}
            </div>
          </div>
        </nav>}
      </div>
    </header>
  )
}

export default Navbar;
