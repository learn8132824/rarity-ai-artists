/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';

const nftSlice = createSlice({
  name: 'users',
  initialState: {
    list: [],
    loading: false,
    error: false,
  },
  reducers: {
    USERS_START: (state) => {
      state.error = null;
      state.loading = true;
    },
    USERS_SUCCESS: (state) => {
      state.loading = false;
    },
    GET_USERS: (state, action) => {
      state.list = action.payload;
      state.loading = false;
    },
    USERS_ERROR: (state, action) => {
      state.loading = false;
      state.error = action.payload;
    },
    RESET_USERS: (state) => {
      state.list = [];
    },
  },
});

export const {
  USERS_START,
  USERS_SUCCESS,
  GET_USERS,
  USERS_ERROR,
  RESET_USERS,
} = nftSlice.actions;
export default nftSlice.reducer;
