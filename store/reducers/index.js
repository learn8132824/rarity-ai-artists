import { combineReducers } from 'redux';

import nftsSlice from './nftsReducer';
import userSlice from './userReducer';
import usersSlice from './usersReducer';

export default combineReducers({
  user: userSlice,
  users: usersSlice,
  nfts: nftsSlice,
});
